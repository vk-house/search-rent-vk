<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Security\Core\SecurityContext;

class RegistrationFormType extends AbstractType
{
    private $class;
    private $securityContext;
    private $doctrine;

    /**
     * @param string $class The User class name
     */
    public function __construct($class, SecurityContext $securityContext, $doctrine)
    {
        $this->class = $class;
        $this->securityContext = $securityContext;
        $this->doctrine = $doctrine;
    }

    public function buildForm(FormBuilderInterface $builder, array $options){

        $builder
            ->add('username', 'email', array('label' => 'Логин:', 'translation_domain' => 'FOSUserBundle', 'attr' => array(
                'placeholder'=>'Введите e-mail',
                )))
            ->add('plainPassword', 'repeated',  array(
                'attr' => array('placeholder'=>'Введите пароль'),
                'type' => 'password',
                'options' => array('translation_domain' => 'FOSUserBundle'),
                'first_options' => array('label' => 'form.password'),
                'second_options' => array('label' => 'form.password_confirmation'),
                'invalid_message' => 'fos_user.password.mismatch',
            ))

            ->add('firstName', null, array('label'=>'Имя:','attr' => array(
                'placeholder'=>'',
                )))
            ->add('lastName', null, array('label'=>'Фамилия:','required'=>false, 'attr' => array(
                'placeholder'=>'',
                )))
            ->add('email', null, array('label'=>'Имя:','attr' => array(
                'placeholder'=>'',
            )))
            ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => $this->class,
            'intention'  => 'registration',
        ));
    }

    public function getName()
    {
        return 'admin_user_registration';
    }
}
