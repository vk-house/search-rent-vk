<?php

namespace BackEndBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TokenVk
 *
 * @ORM\Table(name="token_vk")
 * @ORM\Entity(repositoryClass="BackEndBundle\Repository\TokenVkRepository")
 */
class TokenVk
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="access_token", type="string", length=255)
     */
    private $accessToken;

    /**
     * @var string
     *
     * @ORM\Column(name="expid_time", type="string", length=255)
     */
    private $expidTime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_generate", type="datetime", length=255)
     */
    private $dateGenerate;

    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set accessToken
     *
     * @param string $accessToken
     * @return TokenVk
     */
    public function setAccessToken($accessToken)
    {
        $this->accessToken = $accessToken;

        return $this;
    }

    /**
     * Get accessToken
     *
     * @return string 
     */
    public function getAccessToken()
    {
        return $this->accessToken;
    }

    /**
     * Set expidTime
     *
     * @param string $expidTime
     * @return TokenVk
     */
    public function setExpidTime($expidTime)
    {
        $this->expidTime = $expidTime;

        return $this;
    }

    /**
     * Get expidTime
     *
     * @return string 
     */
    public function getExpidTime()
    {
        return $this->expidTime;
    }

    /**
     * Set dateGenerate
     *
     * @param \DateTime $dateGenerate
     * @return TokenVk
     */
    public function setDateGenerate($dateGenerate)
    {
        $this->dateGenerate = $dateGenerate;

        return $this;
    }

    /**
     * Get dateGenerate
     *
     * @return \DateTime 
     */
    public function getDateGenerate()
    {
        return $this->dateGenerate;
    }
}
