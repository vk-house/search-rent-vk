<?php

namespace BackEndBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Rent
 *
 * @ORM\Table(name="rent")
 * @ORM\Entity(repositoryClass="BackEndBundle\Repository\RentRepository")
 */
class Rent
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="text", nullable=true)
     */
    private $text;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_added", type="datetime")
     */
    private $dateAdded;

    /**
     * @var integer
     *
     * @ORM\Column(name="vk_id", type="integer", nullable=true)
     */
    private $vkId;

    /**
     * @var integer
     *
     * @ORM\Column(name="owner_id", type="integer", nullable=true)
     */
    private $ownerId;

    /**
     * @var integer
     *
     * @ORM\Column(name="signer_id", type="integer", nullable=true)
     */
    private $signerId;

    /**
     * показатель или прошло модерацию объявление
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=true, options={"default" : "0"})
     */
    private $active;

    /**
     * @var string
     *
     * @ORM\Column(name="text_no_tags", type="text", nullable=true, options={"default" : "0"})
     */
    private $textNoTags;


    /**
     * первое фото
     * @var string
     *
     * @ORM\Column(name="main_photo", type="string", nullable=true)
     */
    private $mainPhoto;

    /**
     * @ORM\ManyToOne(targetEntity="Metro", inversedBy="rent")
     * @ORM\JoinColumn(name="metro", referencedColumnName="id" )
     */
    protected $metro;

    /**
     * @ORM\OneToMany(targetEntity="Photo", mappedBy="rent")
     * @var ArrayCollection $photo
     */
    protected $photo;

    /**
     * определитель чем является объявление спросом или же предложением, по деволту предложение
     * @var boolean
     *
     * @ORM\Column(name="demand", type="boolean", nullable=true, options={"default" : "0"})
     */
    private $demand;

    /**
     * количество лайков
     * @var integer
     *
     * @ORM\Column(name="like_amount", type="integer", nullable=true, options={"default" : "0"})
     */
    private $likeAmount;

    /**
     * количество комментов
     * @var integer
     *
     * @ORM\Column(name="comment_amount", type="integer", nullable=true, options={"default" : "0"})
     */
    private $commentAmount;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=150, nullable=true)
     */
    private $address;

    /**
     * @var integer
     *
     * @ORM\Column(name="price", type="integer", nullable=true)
     */
    private $price;

    /**
     * @ORM\ManyToOne(targetEntity="TypeRent", inversedBy="rent")
     * @ORM\JoinColumn(name="type_rent", referencedColumnName="id" )
     */
    protected $typeRent;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->photo = new \Doctrine\Common\Collections\ArrayCollection();
        $this->demand = 0;
        $this->active = 0;
        $this->likeAmount = 0;
        $this->commentAmount = 0;
        $this->dateAdded = new \DateTime(date('Y-m-d H:i:s', time() ));
    }

    public function getHashTags() {
        $hashTags = array();
        if (is_object ($this->typeRent)) {
            array_push($hashTags, array('name' => $this->typeRent->getType(), 'id' => $this->typeRent->getId(), 'type' => 'room') );
        }
        if (is_object($this->metro)) {
            array_push($hashTags, array('name' => $this->metro->getName(), 'id' => $this->metro->getId(), 'type' => 'metro') );
        }
        return $hashTags;
    }


    public static function createRent($ParsingText, $textNoTags, $dateAdded, $vkId, $ownerId, $active, $metro, $demand, $TypeRent, $price, $signerId, $commentAmount, $mainPhoto, $likeAmount) {
        $rentCreate = new Rent();
        $rentCreate->setText($ParsingText);
        $rentCreate->setTextNoTags($textNoTags);
        $rentCreate->setDateAdded($dateAdded);
        $rentCreate->setVkId($vkId);
        $rentCreate->setOwnerId($ownerId);
        $rentCreate->setActive($active);
        $rentCreate->setMetro($metro);
        $rentCreate->setDemand($demand);
        $rentCreate->setTypeRent($TypeRent);
        $rentCreate->setPrice($price);
        $rentCreate->setSignerId($signerId);
        $rentCreate->setCommentAmount($commentAmount);
        $rentCreate->setMainPhoto($mainPhoto);
        $rentCreate->setLikeAmount($likeAmount);
        return $rentCreate;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set text
     *
     * @param string $text
     * @return Rent
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string 
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set dateAdded
     *
     * @param \DateTime $dateAdded
     * @return Rent
     */
    public function setDateAdded($dateAdded)
    {
        $this->dateAdded = $dateAdded;

        return $this;
    }

    /**
     * Get dateAdded
     *
     * @return \DateTime 
     */
    public function getDateAdded()
    {
        return $this->dateAdded;
    }

    /**
     * Set vkId
     *
     * @param integer $vkId
     * @return Rent
     */
    public function setVkId($vkId)
    {
        $this->vkId = $vkId;

        return $this;
    }

    /**
     * Get vkId
     *
     * @return integer 
     */
    public function getVkId()
    {
        return $this->vkId;
    }

    /**
     * Set ownerId
     *
     * @param integer $ownerId
     * @return Rent
     */
    public function setOwnerId($ownerId)
    {
        $this->ownerId = $ownerId;

        return $this;
    }

    /**
     * Get ownerId
     *
     * @return integer 
     */
    public function getOwnerId()
    {
        return $this->ownerId;
    }

    /**
     * Set signerId
     *
     * @param integer $signerId
     * @return Rent
     */
    public function setSignerId($signerId)
    {
        $this->signerId = $signerId;

        return $this;
    }

    /**
     * Get signerId
     *
     * @return integer 
     */
    public function getSignerId()
    {
        return $this->signerId;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Rent
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return (bool) $this->active;
    }

    /**
     * Set textNoTags
     *
     * @param string $textNoTags
     * @return Rent
     */
    public function setTextNoTags($textNoTags)
    {
        $this->textNoTags = $textNoTags;

        return $this;
    }

    /**
     * Get textNoTags
     *
     * @return string 
     */
    public function getTextNoTags()
    {
        return $this->textNoTags;
    }

    /**
     * Set mainPhoto
     *
     * @param string $mainPhoto
     * @return Rent
     */
    public function setMainPhoto($mainPhoto)
    {
        $this->mainPhoto = $mainPhoto;

        return $this;
    }

    /**
     * Get mainPhoto
     *
     * @return string 
     */
    public function getMainPhoto()
    {
        return $this->mainPhoto;
    }

    /**
     * Set demand
     *
     * @param boolean $demand
     * @return Rent
     */
    public function setDemand($demand)
    {
        $this->demand = $demand;

        return $this;
    }

    /**
     * Get demand
     *
     * @return boolean 
     */
    public function getDemand()
    {
        return (bool) $this->demand;
    }

    /**
     * Set likeAmount
     *
     * @param integer $likeAmount
     * @return Rent
     */
    public function setLikeAmount($likeAmount)
    {
        $this->likeAmount = $likeAmount;

        return $this;
    }

    /**
     * Get likeAmount
     *
     * @return integer 
     */
    public function getLikeAmount()
    {
        return $this->likeAmount;
    }

    /**
     * Set commentAmount
     *
     * @param integer $commentAmount
     * @return Rent
     */
    public function setCommentAmount($commentAmount)
    {
        $this->commentAmount = $commentAmount;

        return $this;
    }

    /**
     * Get commentAmount
     *
     * @return integer 
     */
    public function getCommentAmount()
    {
        return $this->commentAmount;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Rent
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set price
     *
     * @param integer $price
     * @return Rent
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return integer 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set metro
     *
     * @param \BackEndBundle\Entity\Metro $metro
     * @return Rent
     */
    public function setMetro(\BackEndBundle\Entity\Metro $metro = null)
    {
        $this->metro = $metro;

        return $this;
    }

    /**
     * Get metro
     *
     * @return \BackEndBundle\Entity\Metro 
     */
    public function getMetro()
    {
        return $this->metro;
    }

    /**
     * Add photo
     *
     * @param \BackEndBundle\Entity\Photo $photo
     * @return Rent
     */
    public function addPhoto(\BackEndBundle\Entity\Photo $photo)
    {
        $this->photo[] = $photo;

        return $this;
    }

    /**
     * Remove photo
     *
     * @param \BackEndBundle\Entity\Photo $photo
     */
    public function removePhoto(\BackEndBundle\Entity\Photo $photo)
    {
        $this->photo->removeElement($photo);
    }

    /**
     * Get photo
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * Set typeRent
     *
     * @param \BackEndBundle\Entity\TypeRent $typeRent
     * @return Rent
     */
    public function setTypeRent(\BackEndBundle\Entity\TypeRent $typeRent = null)
    {
        $this->typeRent = $typeRent;

        return $this;
    }

    /**
     * Get typeRent
     *
     * @return \BackEndBundle\Entity\TypeRent 
     */
    public function getTypeRent()
    {
        return $this->typeRent;
    }
}
