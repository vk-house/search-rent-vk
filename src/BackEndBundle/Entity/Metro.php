<?php

namespace BackEndBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Metro
 *
 * @ORM\Table(name="metro")
 * @ORM\Entity(repositoryClass="BackEndBundle\Repository\MetroRepository")
 */
class Metro
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="sorting", type="integer")
     */
    private $sorting;

    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="boolean")
     */
    private $deleted;

    /**
     * @ORM\ManyToOne(targetEntity="City", inversedBy="metro")
     * @ORM\JoinColumn(name="city", referencedColumnName="id" )
     */
    protected $city;

    /**
     * @ORM\OneToMany(targetEntity="Rent", mappedBy="metro")
     * @var ArrayCollection $rent
     */
    protected $rent;


    public function __toString()
    {
        return $this->name;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Metro
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set sorting
     *
     * @param integer $sorting
     * @return Metro
     */
    public function setSorting($sorting)
    {
        $this->sorting = $sorting;

        return $this;
    }

    /**
     * Get sorting
     *
     * @return integer 
     */
    public function getSorting()
    {
        return $this->sorting;
    }

    /**
     * Set deleted
     *
     * @param integer $deleted
     * @return Metro
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return integer 
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set city
     *
     * @param \BackEndBundle\Entity\City $city
     * @return Metro
     */
    public function setCity(\BackEndBundle\Entity\City $city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return \BackEndBundle\Entity\City 
     */
    public function getCity()
    {
        return $this->city;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->rent = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add rent
     *
     * @param \BackEndBundle\Entity\Rent $rent
     * @return Metro
     */
    public function addRent(\BackEndBundle\Entity\Rent $rent)
    {
        $this->rent[] = $rent;

        return $this;
    }

    /**
     * Remove rent
     *
     * @param \BackEndBundle\Entity\Rent $rent
     */
    public function removeRent(\BackEndBundle\Entity\Rent $rent)
    {
        $this->rent->removeElement($rent);
    }

    /**
     * Get rent
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRent()
    {
        return $this->rent;
    }
}
