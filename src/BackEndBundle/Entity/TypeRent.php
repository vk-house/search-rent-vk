<?php

namespace BackEndBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TypeRent
 *
 * @ORM\Table(name="type_rent")
 * @ORM\Entity(repositoryClass="BackEndBundle\Repository\TypeRentRepository")
 */
class TypeRent
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type_rent", type="string", length=30)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="extra_type", type="string", length=50, nullable=true)
     */
    private $extraType;

    /**
     * @ORM\OneToMany(targetEntity="Rent", mappedBy="type")
     * @var ArrayCollection $rent
     */
    protected $rent;

    public function __toString()
    {
        return $this->type;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return TypeRent
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->rent = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set extraType
     *
     * @param string $extraType
     * @return TypeRent
     */
    public function setExtraType($extraType)
    {
        $this->extraType = $extraType;

        return $this;
    }

    /**
     * Get extraType
     *
     * @return string 
     */
    public function getExtraType()
    {
        return $this->extraType;
    }

    /**
     * Add rent
     *
     * @param \BackEndBundle\Entity\Rent $rent
     * @return TypeRent
     */
    public function addRent(\BackEndBundle\Entity\Rent $rent)
    {
        $this->rent[] = $rent;

        return $this;
    }

    /**
     * Remove rent
     *
     * @param \BackEndBundle\Entity\Rent $rent
     */
    public function removeRent(\BackEndBundle\Entity\Rent $rent)
    {
        $this->rent->removeElement($rent);
    }

    /**
     * Get rent
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRent()
    {
        return $this->rent;
    }
}
