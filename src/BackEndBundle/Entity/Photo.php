<?php

namespace BackEndBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Photo
 *
 * @ORM\Table(name="photo")
 * @ORM\Entity(repositoryClass="BackEndBundle\Repository\PhotoRepository")
 */
class Photo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=10, nullable=true)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="small_photo", type="string", length=200, nullable=true)
     */
    private $smallPhoto;

    /**
     * @var string
     *
     * @ORM\Column(name="medium_photo", type="string", length=200, nullable=true)
     */
    private $mediumPhoto;

    /**
     * @var string
     *
     * @ORM\Column(name="big_photo", type="string", length=200, nullable=true)
     */
    private $bigPhoto;


    /**
     * @ORM\ManyToOne(targetEntity="Rent", inversedBy="photo")
     * @ORM\JoinColumn(name="rent", referencedColumnName="id" )
     */
    protected $rent;

    // создаем фото с задаными значениями
    public static function createPhoto(Rent $Rent, $urlForSmallPhoto, $urlForBigPhoto, $urlForExtraBigPhoto) {
        $Photo = new Photo();
        $Photo->setRent($Rent);
        $Photo->setSmallPhoto($urlForSmallPhoto);
        $Photo->setMediumPhoto($urlForBigPhoto);
        $Photo->setBigPhoto($urlForExtraBigPhoto);
        return $Photo;
    }

    public function __toString()
    {
        return $this->smallPhoto;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Photo
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Photo
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }


    /**
     * Set rent
     *
     * @param \BackEndBundle\Entity\Rent $rent
     * @return Photo
     */
    public function setRent(\BackEndBundle\Entity\Rent $rent = null)
    {
        $this->rent = $rent;

        return $this;
    }

    /**
     * Get rent
     *
     * @return \BackEndBundle\Entity\Rent 
     */
    public function getRent()
    {
        return $this->rent;
    }

    /**
     * Set smallPhoto
     *
     * @param string $smallPhoto
     * @return Photo
     */
    public function setSmallPhoto($smallPhoto)
    {
        $this->smallPhoto = $smallPhoto;

        return $this;
    }

    /**
     * Get smallPhoto
     *
     * @return string 
     */
    public function getSmallPhoto()
    {
        return $this->smallPhoto;
    }

    /**
     * Set mediumPhoto
     *
     * @param string $mediumPhoto
     * @return Photo
     */
    public function setMediumPhoto($mediumPhoto)
    {
        $this->mediumPhoto = $mediumPhoto;

        return $this;
    }

    /**
     * Get mediumPhoto
     *
     * @return string 
     */
    public function getMediumPhoto()
    {
        return $this->mediumPhoto;
    }

    /**
     * Set bigPhoto
     *
     * @param string $bigPhoto
     * @return Photo
     */
    public function setBigPhoto($bigPhoto)
    {
        $this->bigPhoto = $bigPhoto;

        return $this;
    }

    /**
     * Get bigPhoto
     *
     * @return string 
     */
    public function getBigPhoto()
    {
        return $this->bigPhoto;
    }
}
