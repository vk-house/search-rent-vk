<?php

namespace BackEndBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GroupVk
 *
 * @ORM\Table(name="group_vk")
 * @ORM\Entity(repositoryClass="BackEndBundle\Repository\GroupVkRepository")
 */
class GroupVk
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name_group", type="string", length=255, nullable=true)
     */
    private $nameGroup;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255)
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\Column(name="id_group", type="string", length=255)
     */
    private $idGroup;

    /**
     * @var boolean
     *
     * @ORM\Column(name="archive", type="boolean", options={"default" : "0"})
     */
    private $archive;

    /**
     * Constructor
     */
    public function __construct() {
        $this->archive = 0;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nameGroup
     *
     * @param string $nameGroup
     * @return GroupVk
     */
    public function setNameGroup($nameGroup)
    {
        $this->nameGroup = $nameGroup;

        return $this;
    }

    /**
     * Get nameGroup
     *
     * @return string 
     */
    public function getNameGroup()
    {
        return $this->nameGroup;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return GroupVk
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set idGroup
     *
     * @param string $idGroup
     * @return GroupVk
     */
    public function setIdGroup($idGroup)
    {
        $this->idGroup = $idGroup;

        return $this;
    }

    /**
     * Get idGroup
     *
     * @return string 
     */
    public function getIdGroup()
    {
        return $this->idGroup;
    }

    /**
     * Set archive
     *
     * @param boolean $archive
     * @return GroupVk
     */
    public function setArchive($archive)
    {
        $this->archive = $archive;

        return $this;
    }

    /**
     * Get archive
     *
     * @return boolean 
     */
    public function getArchive()
    {
        return (boolean)$this->archive;
    }
}
