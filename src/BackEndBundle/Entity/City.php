<?php

namespace BackEndBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * City
 *
 * @ORM\Table(name="city")
 * @ORM\Entity(repositoryClass="BackEndBundle\Repository\CityRepository")
 */
class City
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="Metro", mappedBy="city")
     * @var ArrayCollection $metro
     */
    protected $metro;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return City
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->metro = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add metro
     *
     * @param \BackEndBundle\Entity\Metro $metro
     * @return City
     */
    public function addMetro(\BackEndBundle\Entity\Metro $metro)
    {
        $this->metro[] = $metro;

        return $this;
    }

    /**
     * Remove metro
     *
     * @param \BackEndBundle\Entity\Metro $metro
     */
    public function removeMetro(\BackEndBundle\Entity\Metro $metro)
    {
        $this->metro->removeElement($metro);
    }

    /**
     * Get metro
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMetro()
    {
        return $this->metro;
    }
}
