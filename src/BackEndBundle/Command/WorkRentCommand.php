<?php

namespace BackEndBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;


class WorkRentCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
            ->setName('rent:vk')
            ->setDescription('get method')
            ->addOption('method', null, InputOption::VALUE_OPTIONAL, 'get vk wall')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output){
        switch($input->getOption('method')) {
            case 'getWall' :
                $this->getWall();
                break;
            case 'unactiveRent' :
                $this->unactiveRent();
                break;
            case 'deleteRent' :
                $this->deleteRent();
                break;
            default:
                return null;
        }

    }

    private function getWall() {
        set_time_limit (500);
        $em = $this->getContainer()->get('doctrine')->getManager();

        $apiVk = $this->getContainer()->get('service.vkapi');
        $apiVk->setUrl('https://api.vk.com/method');

        $insertDataVk = $this->getContainer()->get('service.vk_utility');

        $stations = $em->getRepository('BackEndBundle:Metro')->findAll();
        $notParsingWord = $em->getRepository('BackEndBundle:ParserWord')->getAllWords();
        $groups_vk = $em->getRepository('BackEndBundle:GroupVk')->findBy(array('archive' => 0));
        $roomsInRent = $em->getRepository('BackEndBundle:TypeRent')->findAll();

        foreach ($groups_vk as $key => $group_vk) {

            $parameters = array(
                'owner_id' => $group_vk->getIdGroup() * (-1),
                'domain' => $group_vk->getUrl(),
                'offset' => 0,
                'count' => 20,
                'filter' => 'all',
            );

            $result = $apiVk->getWall($parameters);

            if ($insertDataVk->ExistElementInArray($result, 'response') ) {
                $insertDataVk->InsertData($result['response'], $stations, $notParsingWord, $roomsInRent);
            }
        }
    }

    private function unactiveRent() {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $vk_rents = $em->getRepository('BackEndBundle:Rent')->findby(array('active' => 1));
        foreach ($vk_rents as $rent) {
            if (strtotime($rent->getDateAdded()->format('Y-m-d H:m:s')) < time()-346000 ) {
                $rent->setActive(0);
                $em->persist($rent);
            }
        }
        $em->flush();
    }

    private function deleteRent() {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $vk_rents = $em->getRepository('BackEndBundle:Rent')->findby(array('active' => 0));
        foreach ($vk_rents as $rent) {
            if (strtotime($rent->getDateAdded()->format('Y-m-d H:m:s')) < time()-60500 ) {
                foreach($rent->getPhoto() as $photo) {
                    $em->remove($photo);
                }
                $em->remove($rent);
            }
        }
        $em->flush();
    }
}





