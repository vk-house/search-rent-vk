<?php
namespace BackEndBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\Filter\ChoiceType;

class GroupVkAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('nameGroup', null, array('label' => 'Название группы', 'attr'=>array('title' => 'название группы, можно в вольном стиле', 'required' => true )) )
            ->add('url', null, array('label' => 'Url группы', 'attr'=>array('title' => 'писать без https и не дай бог кто туда эту хуету впехнет', 'required' => true )) )
            ->add('idGroup', null, array('label' => 'Id группы', 'attr'=>array('title' => 'только целое число', 'required' => true )) )
            ->add('archive', 'checkbox', array('required' => false )
            );

    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('nameGroup')
            ->add('url')
            ->add('idGroup')
            ->add('archive');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('nameGroup')
            ->addIdentifier('url')
            ->addIdentifier('idGroup')
            ->addIdentifier('archive',  ChoiceType::class, array(
        'choices'  => array(
            true => 'Yes',
            false => 'No',
        )));

    }

}