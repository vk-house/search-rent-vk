<?php
namespace BackEndBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\Filter\ChoiceType;

class RentAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('text', 'textarea', array('label' => 'Объявление', 'required' => true, 'attr'=>array('title' => 'текст объявления' )) )
            ->add('dateAdded', 'datetime', array('label' => 'дата поста', 'required' => true, 'attr'=>array('title' => 'дата создания поста')) )
            ->add('vkId', null, array('label' => 'Id записи в ВК', 'required' => true, 'attr'=>array('title' => 'только целое число' )) )
            ->add('ownerId', 'integer', array('label' => 'Id группы в ВК', 'required' => true, 'attr'=>array('title' => 'только целое число' )) )
            ->add('signerId', 'integer', array('label' => 'Id владельца поста', 'required' => true, 'attr'=>array('title' => 'только целое число' )) )
            ->add('active',  'checkbox', array('label' => 'Активно?', 'required' => false, 'attr'=>array('title' => 'да/нет' )) )
            ->add('textNoTags', 'textarea', array('label' => 'Объявление без html тегов', 'required' => true, 'attr'=>array('title' => 'текст объявления')) )
            ->add('mainPhoto', null, array('label' => 'Ссылка на превьющку', 'required' => false, 'attr'=>array('title' => 'полный урл на превьющку' )) )
            ->add('metro', null, array('label' => 'Метро', 'required' => false, 'attr'=>array('title' => 'метро' )) )
            ->add('photo', null, array('label' => 'Фотки', 'required' => false, 'attr'=>array('title' => 'фотки' )) )
            ->add('demand', 'checkbox', array('label' => 'Спрос?',  'required' => false, 'attr'=>array('title' => 'является ли объявка спросом' )) )
            ->add('likeAmount', null, array('label' => 'количество лайков', 'required' => false,  'attr'=>array('title' => 'количество лайков' )) )
            ->add('commentAmount', null, array('label' => 'количество комментариев', 'required' => false, 'attr'=>array('title' => 'количество комментариев' )) )
            ->add('typeRent', null, array('label' => 'Тип квартиры', 'required' => false, 'attr'=>array('title' => 'тип квартиры' )) )
        ;


    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('text')
            ->add('active')
            ->add('textNoTags')
            ->add('metro')
            ->add('demand')
            ->add('likeAmount')
            ->add('commentAmount')
            ->add('typeRent')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('text')
            ->addIdentifier('textNoTags')
            ->addIdentifier('metro')
            ->addIdentifier('likeAmount')
            ->addIdentifier('commentAmount')
            ->addIdentifier('active',  ChoiceType::class, array(
                'choices'  => array(
                    true => 'Yes',
                    false => 'No',
                )))
            ->addIdentifier('demand',  ChoiceType::class, array(
                'choices'  => array(
                    true => 'Yes',
                    false => 'No',
                )))
            ->addIdentifier('typeRent')
        ;

    }

}