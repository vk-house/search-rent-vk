<?php
namespace BackEndBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\Filter\ChoiceType;

class TypeRentAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('type', null, array('label' => 'тип квартиры',  'required' => true, 'attr'=>array('title' => 'количество комнат в квартире' ) ))
            ->add('extraType', null, array('label' => 'тип квартиры',  'required' => true, 'attr'=>array('title' => 'количество комнат в квартире' ) ))
        ;

    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('type')
            ->add('extraType')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('type')
            ->addIdentifier('extraType')
        ;
    }

}