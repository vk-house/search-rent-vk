<?php

namespace BackEndBundle\Interfaces;

interface ApiAdapter {

    public function initializationCurl($url);

    public function OAuthCodeFlow($parameters);

    public function OAuthClientCredentialsFlow($parameters);

    public function getAccessToken($parameters);

    public function getUrl();

    public function setUrl($url);
}