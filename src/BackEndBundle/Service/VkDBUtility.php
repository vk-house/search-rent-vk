<?php

namespace BackEndBundle\Service;

use BackEndBundle\Entity\Photo;
use BackEndBundle\Entity\Rent;

class VkDBUtility
{
    public $globalKernel;

    protected static $instance = null;

    private function __construct() {
        global $kernel;
        $this->globalKernel = $kernel;
    }

    private function __clone() {
    }

    public static function getInstance() {
        if (self::$instance === null ) {
            self::$instance = new VkDBUtility();
        }
        return self::$instance;
    }

    public function InsertData($rents, $stations, $notParsingWord, $roomsInRent)
    {

        $em = $this->globalKernel->getContainer()->get('doctrine')->getEntityManager();

        foreach ($rents as $rent) {
            if (is_array($rent) && $rent['post_type'] == 'post' && $rent['text'] != '' ) {

                if (self::FindBadWord($rent['text']) == true ) {
                    break;
                }
                $textNoSpecialSumbol = str_replace(array('\'','"' ) , array(' ',' ') ,$rent['text']);
                $textNoTags = str_replace(array('<br>','<div>','</div>', '<strong>', '</strong>', '<b>', '</b>') , array(' ', ' ', ' ', ' ', ' ', ' ', ' ') ,$rent['text']);
                $dateCreatePost = new \DateTime(date('Y-m-d H:i:s', $rent['date']) );
                $price = 0;

                if (self::ExistElementInArray($rent, 'attachment') ) {
                    if (self::ExistElementInArray($rent['attachment'],'photo') ) {
                        $mainPhoto = self::ExistElementInArray($rent['attachment']['photo'], 'src_big' );
                    }
                }

                $rentCreate = Rent::createRent($textNoSpecialSumbol,
                    $textNoTags,
                    $dateCreatePost,
                    $rent['id'],
                    $rent['from_id'],
                    1,
                    self::FindMetroInText($textNoTags, $stations),
                    self::FindWordForNotParsing($textNoTags, $notParsingWord),
                    self::FindRoomsInText($textNoTags, $roomsInRent),
                    $price,
                    self::ExistElementInArray($rent, 'signer_id'),
                    self::ExistElementInArray($rent['comments'], 'count'),
                    $mainPhoto,
                    self::ExistElementInArray($rent['likes'],  'count')
                    );

                $em->persist($rentCreate);

                if (self::ExistElementInArray($rent, 'attachments')) {
                    foreach ($rent['attachments'] as $photo ) {
                        $photo = self::InsertPhotos($rentCreate, $photo);
                        if ($photo) {
                            $em->persist($photo);
                        }

                    }
                }

            } else if ($rent['post_type'] == 'copy') {
                $old_rent = $em->getRepository('BackEndBundle:Rent')->findBy(array('vkId' => $rent['id'], 'ownerId' => $rent['from_id'] ));
                if (isset($old_rent[0]) && is_object($old_rent[0])) {
                    $old_rent[0]->setActive(1);
                    $em->persist($old_rent[0]);
                }
            }
        }
        $em->flush();
    }



    public function ExistElementInArray(array $elementForCheck, $nameElement) {
        if (isset($elementForCheck[$nameElement]) ) {
            return $elementForCheck[$nameElement];
        }
        return null;
    }

    public function FindBadWord($textForSearch) {
        $regular_expression = "/\w{0,5}[хx]([хx\s\!@#\$%\^&*+-\|\/]{0,6})[уy]([уy\s\!@#\$%\^&*+-\|\/]{0,6})[ёiлeеюийя]\w{0,7}|\w{0,6}[пp]([пp\s\!@#\$%\^&*+-\|\/]{0,6})[iие]([iие\s\!@#\$%\^&*+-\|\/]{0,6})[3зс]([3зс\s\!@#\$%\^&*+-\|\/]{0,6})[дd]\w{0,10}|[сcs][уy]([уy\!@#\$%\^&*+-\|\/]{0,6})[4чkк]\w{1,3}|\w{0,4}[bб]([bб\s\!@#\$%\^&*+-\|\/]{0,6})[lл]([lл\s\!@#\$%\^&*+-\|\/]{0,6})[yя]\w{0,10}|\w{0,8}[её][bб][лске@eыиаa][наи@йвл]\w{0,8}|\w{0,4}[еe]([еe\s\!@#\$%\^&*+-\|\/]{0,6})[бb]([бb\s\!@#\$%\^&*+-\|\/]{0,6})[uу]([uу\s\!@#\$%\^&*+-\|\/]{0,6})[н4ч]\w{0,4}|\w{0,4}[еeё]([еeё\s\!@#\$%\^&*+-\|\/]{0,6})[бb]([бb\s\!@#\$%\^&*+-\|\/]{0,6})[нn]([нn\s\!@#\$%\^&*+-\|\/]{0,6})[уy]\w{0,4}|\w{0,4}[еe]([еe\s\!@#\$%\^&*+-\|\/]{0,6})[бb]([бb\s\!@#\$%\^&*+-\|\/]{0,6})[оoаa@]([оoаa@\s\!@#\$%\^&*+-\|\/]{0,6})[тnнt]\w{0,4}|\w{0,10}[ё]([ё\!@#\$%\^&*+-\|\/]{0,6})[б]\w{0,6}|\w{0,4}[pп]([pп\s\!@#\$%\^&*+-\|\/]{0,6})[иeеi]([иeеi\s\!@#\$%\^&*+-\|\/]{0,6})[дd]([дd\s\!@#\$%\^&*+-\|\/]{0,6})[oоаa@еeиi]([oоаa@еeиi\s\!@#\$%\^&*+-\|\/]{0,6})[рr]\w{0,12}/i";
        $bad_word = mb_ereg_match($regular_expression, $textForSearch);
        if ($bad_word == true) {
            return true;
        }
        return false;
    }

    public function FindMetroInText($textForSearch, $stations) {
        foreach ($stations as $station) {
            $search_string = mb_substr($station->getName(), 0, -3);
            if (mb_strripos($textForSearch, $search_string) !== false ) {
                return $station;
            }
        }
        return null;
    }

    public function FindWordForNotParsing($textForSearch, $wordForNotParsing) {
        foreach ($wordForNotParsing as $word) {
            if (mb_strripos($textForSearch, $word['word']) !== false) {
                return true;
            }
        }
        return false;
    }

    public function FindRoomsInText($textForSearch, $roomsInRent) {
        foreach ($roomsInRent as $countRoom) {
            if (mb_strripos($textForSearch, $countRoom->getType()) !== false) {
                return $countRoom;
            }
            $extraNamesRent = explode(';', $countRoom->getExtraType());
            foreach ($extraNamesRent as $extraName) {
                if (mb_strripos($textForSearch, $extraName) !== false) {
                    return $countRoom;
                }
            }
        }
        return null;
    }

    public function FindPriceInText($textForSearch) {
        preg_match_all('|\d+|', $textForSearch, $matches);
        $priceFlag = array('руб.', 'рублей');
        foreach ($priceFlag as $current) {
            if (mb_strripos($textForSearch, $current) !== false) {
                if (isset($matches[3]) && isset($matches[4]) )
                return array($matches[3], $matches[4]);
            }
        }
        if (mb_strripos($textForSearch, 'цена') !== false) {
            return $matches[0];
        }

        return null;
    }

    public function InsertPhotos(Rent $Rent, array $photo) {
        if (self::ExistElementInArray($photo, 'photo' )) {
            $Photo = Photo::createPhoto(
                $Rent,
                $photo['photo']['src_small'],
                $photo['photo']['src_big'],
                self::ExistElementInArray($photo['photo'], 'src_xbig' )
            );
            return $Photo;
        }
    }

}