<?php

namespace BackEndBundle\Service;

use BackEndBundle\Interfaces\ApiAdapter;

class VkApi implements ApiAdapter
{

    private $url;

    protected static $_instance;

    private function __construct() {
    }

    public static function getInstance() {
        if (self::$_instance === null) {
            self::$_instance = new self;
        }

        return self::$_instance;
    }

    private function __clone() {
    }

    private function __wakeup() {
    }

    public function initializationCurl($url) {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 60);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("Pragma: no-cache"));
        $out = curl_exec($curl);
        return $out;
    }

    public function OAuthClientCredentialsFlow($parameters) {
        $url = $this->setParameters(self::getUrl() . '?', $parameters);
        $result = $this->initializationCurl($url);
        return $result;
    }

    public function getAccessToken($parameters) {
        $url = $this->setParameters(self::getUrl() . '?', $parameters);
        $result = $this->initializationCurl($url);
        return json_decode($result, true);
    }

    public function getUrl() {
        return $this->url;
    }

    public function setUrl($url) {
        $this->url = $url;
    }

    public function OAuthCodeFlow($parameters) {
        $url = $this->setParameters(self::getUrl() . '?', $parameters);
        echo $url; die();
        $result = $this->initializationCurl($url);
        return $result;
    }

    public function getWall($parameters) {
        $url = $this->setParameters(self::getUrl() . '/wall.get?', $parameters);
        $result = $this->initializationCurl($url);
        return json_decode($result, true);
    }

    public function getUserData($parameters) {
        $url = $this->setParameters(self::getUrl() . '/users.get?', $parameters);
        $result = $this->initializationCurl($url);
        return $result;
    }

    private function setParameters($url, $parameters) {
        foreach ($parameters as $key => $field ) {
            $url .= '&' . $key . '=' . $field;
        }
        return $url;
    }

}