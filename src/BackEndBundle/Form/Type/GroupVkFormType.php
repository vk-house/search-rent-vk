<?php


namespace BackEndBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class GroupVkFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nameGroup', null, array('label' => 'Название группы', 'attr'=>array('title' => 'название группы, можно в вольном стиле', 'required' => true )) )
            ->add('url', null, array('label' => 'Url группы', 'attr'=>array('title' => 'писать без https и не дай бог кто туда эту хуету впехнет', 'required' => true )) )
            ->add('idGroup', null, array('label' => 'Id группы', 'attr'=>array('title' => 'только целое число', 'required' => true )) )
            ->add('archive', ChoiceType::class, array(
                                                    'choices'  => array(
                                                        true => 'Yes',
                                                        false => 'No',
                                                    ))
            )
            ->add('save', SubmitType::class)
        ;
    }
}
