<?php

namespace BackEndBundle\Controller;

use BackEndBundle\Entity\GroupVk;
use BackEndBundle\Entity\TokenVk;
use BackEndBundle\Form\Type\GroupVkFormType;
use BackEndBundle\Service\Utility;
use BackEndBundle\Service\VkApi;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 *
 * @Route("/ad")
 */
class AdminController extends Controller
{

    /**
     * @Route("/", name="admin_main")
     * @Template()
     */
    public function indexAction()
    {
        return array();
    }

    /**
     * @Route("/vk-groups", name="admin_vk_groups")
     * @Template()
     */
    public function vkGroupsAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $token = $em->getRepository('BackEndBundle:TokenVk')->getLastToken();
        $token['dateReset'] = '';
        if (isset($token[0])) {
            $token = $token[0];
            if (isset($token['expidTime']) ) {
                $token['dateReset'] = date('d.m.Y', strtotime($token['dateGenerate']->format('Y-m-d H:i:s')) + $token['expidTime']);
            }
        }
        $vk_groups = $em->getRepository('BackEndBundle:GroupVk')->findBy(array('archive' => 0), array('id' => 'DESC'));
        return array(
            'token'         => $token,
            'vk_groups'     => $vk_groups,
            'create_group'  => $request->query->get('create_group')
        );
    }

    /**
     * @Route("/vk-auth", name="admin_vk_auth")
     */
    public function vkAuthoAction()
    {
        $apiVk = VkApi::getInstance();
        $apiVk->setUrl('https://oauth.vk.com/authorize');
        $parameters = array(
            'client_id'     => 5427882,
            'display'       => 'page',
            'redirect_uri'  => 'http://rentvk.ru/ad/callback',
            'v'             => '5.52',
            'scope'         => 'wall',
            'response_type' => 'code',
        );
        $code = $apiVk->OAuthCodeFlow($parameters);
        echo $code;
        return new Response($code);
    }

    /**
     * @Route("/callback", name="admin_callback")
     * @Method("GET")
     */
    public function callbackAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $apiVk = VkApi::getInstance();
        $apiVk->setUrl('https://oauth.vk.com/access_token');
        $code = $request->query->get('code');
        $parameters = array(
            'client_id' => 5427882,
            'client_secret' => 'wVYrBxknVrnvquC6vpjl',
            'redirect_uri' => 'http://rentvk.ru/ad/callback',
            'code' => $code,
        );
        $array_access = $apiVk->getAccessToken($parameters);
        if (isset($array_access['access_token']) ) {
            $access_token = $array_access['access_token'];
            setcookie('access_token', $access_token, time() + $array_access['expires_in']);
            $date = date('Y-m-d H:i:s', time());
            $token_vk = new TokenVk();
            $token_vk->setAccessToken($access_token);
            $token_vk->setExpidTime($array_access['expires_in']);
            $token_vk->setDateGenerate(new \DateTime($date));
            $em->persist($token_vk);
            $em->flush();
        }
        return $this->redirect($this->generateUrl('sonata_admin_dashboard'));

    }

    /**
     * @Route("/add-group", name="admin_add_group")
     * @Template()
     */
    public function addGroupAction(Request $request)
    {
        $group_vk = new GroupVk();
        $form_group_vk = $this->createForm(GroupVkFormType::class, $group_vk);
        $form_group_vk->handleRequest($request);
        if ($form_group_vk->isSubmitted() && $form_group_vk->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($group_vk);
            $em->flush();
            return $this->redirect($this->generateUrl('admin_vk_groups', array('message' => 'Группа добавлено, все четенько!' )));
        }
        return array('form' =>$form_group_vk->createView());

    }

    /**
     * @Route("/edit-group/{id}", name="admin_edit_group")
     * @Template()
     */
    public function editGroupAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $group_vk = $em->getRepository('BackEndBundle:GroupVk')->findOneById($id);
        $form_group_vk = $this->createForm(GroupVkFormType::class, $group_vk);
        $form_group_vk->handleRequest($request);
        if ($form_group_vk->isSubmitted() && $form_group_vk->isValid()) {
            $em->persist($group_vk);
            $em->flush();
            return $this->redirect($this->generateUrl('admin_vk_groups', array('message' => 'Группа отредактирована, все четенько!' )));
        }
        return array(
            'form'  => $form_group_vk->createView(),
            'id'    => $id,
        );

    }

    /**
     * @Route("/archive-group", name="admin_archive_group")
     */
    public function archiveGroupAction(Request $request)
    {
        $id = $request->query->get('vk_id');
        $em = $this->getDoctrine()->getManager();
        $group_vk = $em->getRepository('BackEndBundle:GroupVk')->findOneById($id);
        if (is_object($group_vk)) {
            $group_vk->setActive(0);
            $em->persist($group_vk);
            $em->flush();
            return new Response('ok');
        }
        return new Response('bad');
    }


    /**
     * @Route("/get-wall", name="admin_get_wall")
     */
    public function getWallAction(Request $request)
    {
        $apiVk = VkApi::getInstance();
        $access = $request->cookies->get('access_token');
        $apiVk->setUrl('https://api.vk.com/method');
        $parameters = array(
            'owner_id'      => '-57466174',
            'domain'        => 'new.vk.com/yuytnoe_gnezdishko',
            'offset'        => 0,
            'count'         => 20,
            'filter'        => 'all',
        );

        $em = $this->getDoctrine()->getManager();
        $result = $apiVk->getWall($parameters);
        Utility::InsertData($result['response'], $em);
        echo 'good';
    }

}