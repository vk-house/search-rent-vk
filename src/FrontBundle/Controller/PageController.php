<?php

namespace FrontBundle\Controller;

use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\FOSUserEvents;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PageController extends Controller
{
    const MAX_RENT_IN_PAGE = 24;
    /**
     * @Route("/", name="index")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $rents = $em->getRepository('BackEndBundle:Rent')->findBy(array('demand' => 0, 'active' => 1), array('id' => 'DESC'), self::MAX_RENT_IN_PAGE, 0);

        if ($this->has('security.csrf.token_manager')) {
            $csrfToken = $this->get('security.csrf.token_manager')->getToken('authenticate')->getValue();
        } else {
            // BC for SF < 2.4
            $csrfToken = $this->has('form.csrf_provider')
                ? $this->get('form.csrf_provider')->generateCsrfToken('authenticate')
                : null;
        }
        setcookie('csrfToken', $csrfToken, time()+2*3600, '/');
        return array('rents' => $rents);

    }


    // все обхявления
    /**
     * @Route("/flats", name="flats")
     * @Template()
     */
    public function flatsAction()
    {
        $em = $this->getDoctrine()->getManager();
        $rents = $em->getRepository('BackEndBundle:Rent')->findBy(array(), array('id' => 'DESC'));

        return array('rents' => $rents);
    }

    // подробная страница объявления
     /**
     * @Route("/flat/{id}", name="flat")
     * @Template()
     */
    public function flatAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $info = $em->getRepository('BackEndBundle:Rent')->findOneById($id);
        return array('info' => $info);
    }

    // контакты
    /**
     * @Route("/contacts", name="contacts")
     * @Template()
     */
    public function contactsAction()
    {
        return array();
    }

    // о нас
    /**
     * @Route("/o-nas", name="about")
     * @Template()
     */
    public function aboutAction()
    {
        $time =  time();
        $em = $this->getDoctrine()->getManager();
        $count_rent = $em->getRepository('BackEndBundle:Rent')->countActiveRent();
        $count_rent_today = $em->getRepository('BackEndBundle:Rent')->countActiveToday(date('Y-m-d H:i:s', $time-18*60*60));
        $count_group = $em->getRepository('BackEndBundle:GroupVk')->countActiveGroup();
        $count_rent = Utility::getCount($count_rent);
        $count_group = Utility::getCount($count_group);
        $count_today = Utility::getCount($count_rent_today);
        $days = date('t', $time) - date('d', $time);
        return array(
            'count_rent'    => $count_rent,
            'days'          => $days,
            'count_group'   => $count_group,
            'count_today'   => $count_today
        );
    }


    // квартиры по количеству комнат
    /**
     * @Route("/rooms", name="room")
     * @Template()
     */
    public function roomsAction(Request $request)
    {
        $room = $request->query->get('type');
        $em = $this->getDoctrine()->getManager();
        $rents = $em->getRepository('BackEndBundle:Rent')->findBy(array('typeRent' => $room ), array('id' => 'DESC'));
        $type_room = $em->getRepository('BackEndBundle:TypeRent')->findOneBy(array('id' => $room ));
        return array('rents' => $rents, 'type_room' => $type_room);
    }

    // квартиры по станции метро
    /**
     * @Route("/metro", name="metro")
     * @Template()
     */
    public function metroAction(Request $request)
    {
        $metro = $request->query->get('type');
        $em = $this->getDoctrine()->getManager();
        $rents = $em->getRepository('BackEndBundle:Rent')->findBy(array('metro' => $metro ), array('id' => 'DESC'));
        $type_metro = $em->getRepository('BackEndBundle:Metro')->findOneBy(array('id' => $metro ));
        return array('rents' => $rents, 'type_metro' => $type_metro);
    }

    /**
     * @Route("/ajax-scroll", name="page_ajax_scroll")
     */
    public function ajaxForScrollAction(Request $request)
    {
        $page = $request->get('page');
        $type = $request->get('type');
        $value_type = $request->get('value_type');

        $em = $this->getDoctrine()->getManager();
        if (!empty($type) && !empty($value_type) ) {
            $rents = $em->getRepository('BackEndBundle:Rent')->findBy(
                array( $type => $value_type, 'demand' => 0, 'active' => 1),
                array('id' => 'DESC'),
                self::MAX_RENT_IN_PAGE,
                ($page*self::MAX_RENT_IN_PAGE)+1 );
        } else {
            $rents = $em->getRepository('BackEndBundle:Rent')->findBy(
                array('demand' => 0, 'active' => 1),
                array('id' => 'DESC'),
                self::MAX_RENT_IN_PAGE,
                ($page*self::MAX_RENT_IN_PAGE)+1 );
        }
        if (is_array($rents) && count($rents) > 0 ) {
            $html = $this->container->get('templating')->render(
                'FrontBundle:Default:all_flats.html.twig',
                array('rents' => $rents)
            );
            return new Response($html);
        }

        return new Response(null);
    }

    /**
     * @Route("/registration", name ="page_registration")
     * @Template()
     */
    public function registrationAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $manUser = $this->container->get('security.context')->getToken()->getUser();
        /** @var $formFactory \FOS\UserBundle\Form\Factory\FactoryInterface */
        $formFactory = $this->container->get('fos_user.registration.form.factory');
        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->container->get('fos_user.user_manager');
        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->container->get('event_dispatcher');

        $user = $userManager->createUser();
        $user->setEnabled(true);
        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }
        $form = $formFactory->createForm();
        $form->setData($user);
        return array(
            'form'      => $form->createView(),
            'error'     => $request->get('error'),
        );
    }

    /**
     * @Route("/success-registration", name ="page_success_registration")
     * @Template()
     */
    public function successRegistrationAction() {
        return array();
    }

}

