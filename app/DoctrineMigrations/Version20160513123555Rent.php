<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;


class Version20160513123555Rent extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql(
            'CREATE TABLE `rent` (
                `id` INT(11) NOT NULL AUTO_INCREMENT,
                `text` LONGTEXT NULL COLLATE utf8_unicode_ci,
                `date_added` DATETIME NOT NULL,
                `vk_id` INT(11) NULL DEFAULT NULL,
                `owner_id` INT(11) NULL DEFAULT NULL,
                `metro` INT(11) NULL DEFAULT NULL,
                `signer_id` INT(11) NULL DEFAULT NULL,
                `demand` TINYINT(1) NULL DEFAULT 0,
                `main_photo` VARCHAR(255) NULL DEFAULT NULL COLLATE utf8_unicode_ci,
                `like_amount` INT(11) NULL DEFAULT 0,
                `comment_amount` INT(11) NULL DEFAULT 0,
                `text_no_tags` LONGTEXT NULL COLLATE utf8_unicode_ci,
                `active` TINYINT(1) NULL DEFAULT 0,
                `type_rent` INT(11) NULL DEFAULT NULL,
                `address` VARCHAR(150) NULL DEFAULT NULL COLLATE utf8_unicode_ci,
                `price` INT(11) NULL DEFAULT NULL,
                PRIMARY KEY (`id`),
                INDEX `IDX_2784DCC3884E4E1` (`metro`),
                INDEX `IDX_2784DCCD57788C4` (`type_rent`),
                CONSTRAINT `FK_2784DCCD57788C4` FOREIGN KEY (`type_rent`) REFERENCES `type_rent` (`id`),
                CONSTRAINT `FK_2784DCC3884E4E1` FOREIGN KEY (`metro`) REFERENCES `metro` (`id`)
            )
            COLLATE=utf8_unicode_ci
            ENGINE=InnoDB
            AUTO_INCREMENT=1
            ;'
        );

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql('DROP TABLE rent');
    }
}
