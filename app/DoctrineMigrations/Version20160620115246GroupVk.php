<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;


class Version20160620115246GroupVk extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql(
            'CREATE TABLE `group_vk` (
                `id` INT(11) NOT NULL AUTO_INCREMENT,
                `name_group` VARCHAR(255) NULL DEFAULT NULL COLLATE utf8_unicode_ci,
                `url` VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci,
                `id_group` VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci,
                `archive` TINYINT(1) NOT NULL DEFAULT 0,
                PRIMARY KEY (`id`)
            )
            COLLATE=utf8_unicode_ci
            ENGINE=InnoDB
            ;'
        );

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql('DROP TABLE group_vk');

    }
}

