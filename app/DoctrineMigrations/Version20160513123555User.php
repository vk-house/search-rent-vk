<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;


class Version20160513123555User extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql(
            'CREATE TABLE IF NOT EXISTS `user` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                  `username_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                  `email_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                  `enabled` tinyint(1) NOT NULL,
                  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                  `last_login` datetime DEFAULT NULL,
                  `locked` tinyint(1) NOT NULL,
                  `expired` tinyint(1) NOT NULL,
                  `expires_at` datetime DEFAULT NULL,
                  `confirmation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                  `password_requested_at` datetime DEFAULT NULL,
                  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT "(DC2Type:array)",
                  `credentials_expired` tinyint(1) NOT NULL,
                  `credentials_expire_at` datetime DEFAULT NULL,
                  `firstName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                  `LastName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                  `date_reg` datetime NULL,
                  PRIMARY KEY (`id`),
                  UNIQUE KEY `UNIQ_8D93D64992FC23A8` (`username_canonical`),
                  UNIQUE KEY `UNIQ_8D93D649A0D96FBF` (`email_canonical`)
             ) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci'
        );

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql('DROP TABLE `user`');
    }
}
