<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;


class Version20160513124555ParserWord extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql(
            'CREATE TABLE `parser_word` (
                `id` INT(11) NOT NULL AUTO_INCREMENT,
                `word` VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci,
                PRIMARY KEY (`id`)
            )
            COLLATE=utf8_unicode_ci
            ENGINE=InnoDB
            ;'
        );

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql('DROP TABLE parser_word');

    }
}

