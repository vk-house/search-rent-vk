<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;


class Version20160513123553TypeRent extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql(
            'CREATE TABLE `type_rent` (
                `id` INT(11) NOT NULL AUTO_INCREMENT,
                `type_rent` VARCHAR(30) NOT NULL COLLATE utf8_unicode_ci,
                `extra_type` VARCHAR(50) NULL DEFAULT NULL COLLATE utf8_unicode_ci,
                PRIMARY KEY (`id`)
            )
            COLLATE=utf8_unicode_ci
            ENGINE=InnoDB
            ;'
        );

        $this->addSql(
            'INSERT INTO `type_rent` (`id`, `type_rent`, `extra_type`) VALUES
	          (1, "1 комната", "1-к квартира"),
	          (2, "2 комнаты", "2-к квартира"),
	          (3, "3 комнаты", "3-к квартира"),
	          (4, "студия", ""),
	          (5, "комната", "")
	          '
        );

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql('DROP TABLE type_rent');

    }
}
