<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;


class Version20160620115246TokenVk extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql(
            'CREATE TABLE `token_vk` (
                `id` INT(11) NOT NULL AUTO_INCREMENT,
                `access_token` VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci,
                `expid_time` VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci,
                `date_generate` DATETIME NOT NULL,
                PRIMARY KEY (`id`)
            )
            COLLATE=utf8_unicode_ci
            ENGINE=InnoDB
            ;'
        );

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql('DROP TABLE token_vk');

    }
}
