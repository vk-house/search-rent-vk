<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;


class Version20160513123555RentForPhoto extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql(
            'CREATE TABLE `photo` (
                `id` INT(11) NOT NULL AUTO_INCREMENT,
                `name` VARCHAR(255) NULL DEFAULT NULL COLLATE utf8_unicode_ci,
                `type` VARCHAR(10) NULL DEFAULT NULL COLLATE utf8_unicode_ci,
                `rent` INT(11) NULL DEFAULT NULL,
                `small_photo` VARCHAR(200) NULL DEFAULT NULL COLLATE utf8_unicode_ci,
                `medium_photo` VARCHAR(200) NULL DEFAULT NULL COLLATE utf8_unicode_ci,
                `big_photo` VARCHAR(200) NULL DEFAULT NULL COLLATE utf8_unicode_ci,
                PRIMARY KEY (`id`),
                INDEX `IDX_14B784182784DCC` (`rent`),
                CONSTRAINT `FK_14B784182784DCC` FOREIGN KEY (`rent`) REFERENCES `rent` (`id`)
            )
            COLLATE=utf8_unicode_ci
            ENGINE=InnoDB
            ;'
        );

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql('DROP TABLE photo');
    }
}
